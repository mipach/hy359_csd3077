"use strict";

var TIV3077  =function() {
	var files;
	var k = 0;
	var latitude;
	var longtitude;
	var string = "";
	return{
		loadImages: function(){
			files = document.getElementById("images").files;
		},
		getLoadedImages: function(){
			return files;
		},
		showLoadedImages: function(elem){
			//if there is an error message remove it
			document.getElementById(elem).innerHTML = "";
			document.getElementById("map").innerHTML = "";
			document.getElementById("map").style = "width:400px;height:200px;";
			var ele = document.getElementById('no_images');
			if (ele) {
				ele.remove();
			}
			//if no images print the error message
			if (files === undefined) {
				var divv = document.createElement('div');
				divv.id = "no_images";
				divv.innerHTML = ["<h1>NO IMAGES LOADED</h1>"];
				document.getElementById(elem).insertBefore(divv,null);
			}
			else{
				//print the images
				k = 0;
				for (var i = 0; i < files.length; i++){
					k = i;
					var file = files[i];
					var reader = new FileReader();
					reader.onload = (function(file,i){
						return function(e){
						var divv = document.createElement('div');
						divv.className = "responsive";
						divv.innerHTML = ['<div class="boxInner">','<img src="',e.target.result,'" onclick="TIV3077.showImageDetailedExifWithMap(',i,',\'container\')"><div class="title">',file.name.split('.').shift(),'</div>','</div>'].join('');
						document.getElementById(elem).insertBefore(divv,null);
						};
					})(file,i);
					reader.readAsDataURL(file);
				}
			}
		},
		showImage: function(index,elem){
			var file = files[index];
			var reader = new FileReader();
			reader.onload = (function (file) {
				return function (e) {
					var divv = document.createElement('div');
					divv.innerHTML += ['<img src="',e.target.result ,'" id="largeImage" onclick="TIV3077.showLoadedImages(\'container\');">' ].join('');
					document.getElementById(elem).insertBefore(divv,null);
				}
			})(file);
			reader.readAsDataURL(file);		
		},
		showImageDetailedExifInfo: function(index,elem){
			var file = files[index];
			EXIF.getData(file,function () {
				var allMetaData = EXIF.getAllTags(this);
				var divv = document.getElementById(elem);
				divv.innerHTML +=['<div id="metadata" onclick="TIV3077.showLoadedImages(\'container\');">',JSON.stringify(allMetaData,null,"\t"),'</div>' ].join('');
			})		
		},
		initMap: function() {
			  	var uluru = {lat: Number(latitude[2]),lng:Number(longtitude[2])}; 
			  	 var map = new google.maps.Map(document.getElementById("map"),{zoom:4,center:uluru});
			  	 var marker = new google.maps.Marker({position: uluru,map:map});
		},
		showImageDetailedExifWithMap: function (index,elem) {
			document.getElementById(elem).innerHTML = "";
			var file = files[index];
			EXIF.getData(file,function () {
				latitude = EXIF.getTag(file,"GPSLatitude");
				longtitude = EXIF.getTag(file,"GPSLongitude");
				
			  	var script = document.createElement('script');
			  	script.src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuBaiB4jOBIYzMXeKl12ibCN6fAsvRK0I&callback=TIV3077.initMap";
			  	document.getElementById(elem).appendChild(script);	
			})	
			TIV3077.showImage(index,elem);
			TIV3077.showImageDetailedExifInfo(index,elem);
		}

	};
}();