/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import ask3.User;
//import ask3.UserDB;

import cs359db.db.*;
import cs359db.model.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.regex.*;
/**
 *
 * @author Michalis
 */

@WebServlet(urlPatterns = {"/Register"})
public class Register extends HttpServlet {

    public static UserDB db;

    @Override
    public void init() {
        db = new UserDB();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        
        if (request.getParameter("UName") != null && ((UserDB.checkValidUserName(request.getParameter("UName")))) && request.getParameter("UName").matches(".{7,}")
                && request.getParameter("Mail") != null && request.getParameter("Mail").matches("^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$")
                && request.getParameter("Passwd") != null && request.getParameter("Passwd").matches("^(?=.*\\d)(?=.*[!@#$%^&*()_])(?=.*[a-zA-Z]).{5,10}$")
                && request.getParameter("VPasswd") != null && request.getParameter("Passwd").equals(request.getParameter("VPasswd"))
                && request.getParameter("Name") != null && request.getParameter("Name").matches("[a-zA-zΑ-Ωα-ω]{2,20}")
                && request.getParameter("LName") != null && request.getParameter("LName").matches("[a-zA-zΑ-Ωα-ω]{2,20}")
                && request.getParameter("bday") != null
                && request.getParameter("gender") != null
                && request.getParameter("country") != null 
                && request.getParameter("hometown") != null && request.getParameter("hometown").matches(".{2,50}")
                ) {
            User us = new User(request.getParameter("UName"),request.getParameter("Mail"), request.getParameter("Passwd"),  request.getParameter("Name"),
                    request.getParameter("LName"), request.getParameter("bday"), request.getParameter("country"),
                    request.getParameter("hometown"));
            us.setGender(request.getParameter("gender"));
            us.setInfo(request.getParameter("Information"));

            try (PrintWriter out = response.getWriter()) {

                out.println("Your registration was successfully completed <br>");
                out.println("Username:"+ServletUtilities.filter(request.getParameter("UName"))+"<br>");
                out.println("Mail:"+ServletUtilities.filter(request.getParameter("Mail"))+"<br>");
                out.println("Password:"+ServletUtilities.filter(request.getParameter("Passwd"))+"<br>");
                out.println("First Name:"+ServletUtilities.filter(request.getParameter("Name"))+"<br>");
                out.println("Last Name:"+ServletUtilities.filter(request.getParameter("LName"))+"<br>");
                out.println("Birth Day:"+request.getParameter("bday")+"<br>");
                out.println("Gender:"+request.getParameter("gender")+"<br>");
                out.println("Country:"+request.getParameter("country")+"<br>");
                out.println("hometown:"+ServletUtilities.filter(request.getParameter("hometown"))+"<br>");
                out.println("Information:"+ServletUtilities.filter(request.getParameter("Information"))+"<br>");
        
                UserDB.addUser(us);

               

            }
        }
        else
        {
                PrintWriter out = response.getWriter();
                out.println("<h1>Error at Form</h1>");
                out.println("Username:"+ServletUtilities.filter(request.getParameter("UName"))+"<br>");
                out.println("Mail:"+ServletUtilities.filter(request.getParameter("Mail"))+"<br>");
                out.println("Password:"+ServletUtilities.filter(request.getParameter("Passwd"))+"<br>");
                out.println("First Name:"+ServletUtilities.filter(request.getParameter("Name"))+"<br>");
                out.println("Last Name:"+ServletUtilities.filter(request.getParameter("LName"))+"<br>");
                out.println("Birth Day:"+request.getParameter("bday")+"<br>");
                out.println("Gender:"+request.getParameter("gender")+"<br>");
                out.println("Country:"+request.getParameter("country")+"<br>");
                out.println("hometown:"+ServletUtilities.filter(request.getParameter("hometown"))+"<br>");
                out.println("Information:"+ServletUtilities.filter(request.getParameter("Information"))+"<br>");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
