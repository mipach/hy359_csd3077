/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';

function validate(field,query)
{
    
    if (field === "uname")
    {
        var name = document.getElementById(field).value;
        if(name.length < 7)
            document.getElementById(query).innerHTML = "Username too short";
        else
            document.getElementById(query).innerHTML = '';
    }
    if(field === "mail")
    {
        var mail = document.getElementById(field).value;
        if (!mail.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
        {
            document.getElementById(query).innerHTML = "Invalid e-mail Address";
        }
        else
            document.getElementById(query).innerHTML = '';
    }
    if (field ===  'passwd') {
        var pass = document.getElementById(field).value;
        if(!pass.match(/^(?=.*\d)(?=.*[!@#$%^&*()_])(?=.*[a-zA-Z]).{5,10}$/))
            document.getElementById(query).innerHTML = "Invalid Password";
        else
            document.getElementById(query).innerHTML = '';
    }
    if(field === 'vpasswd')
    {
        var vpass = document.getElementById(field).value;
        var pass = document.getElementById("passwd").value;
        if(pass !== vpass)
            document.getElementById(query).innerHTML = 'Passwords must agree';
        else
            document.getElementById(query).innerHTML = '';
    }
    if (field === "name") {
        var name = document.getElementById(field).value;
        if (!name.match(/[a-zA-zΑ-Ωα-ω]{3,20}/))
            document.getElementById(query).innerHTML = 'Invalid First Name';
        else
            document.getElementById(query).innerHTML = '';
    }
    if (field ==='lname')
    {
        var name = document.getElementById(field).value;
        if (!name.match(/[a-zA-zΑ-Ωα-ω]{4,20}/))
            document.getElementById(query).innerHTML = 'Invalid Last Name';
        else
            document.getElementById(query).innerHTML = '';
    }
    if (field === 'HomeTown')  
    {
        var name = document.getElementById(field).value;
        if (!name.match(/.{2,50}/))
            document.getElementById(query).innerHTML = 'Invalid Town';
        else
            document.getElementById(query).innerHTML = '';
    }
    if (field === 'information')
    {
        var name = document.getElementById(field).value;
        if(name.length > 500)
            document.getElementById(query).innerHTML = "Information too long";
        else
            document.getElementById(query).innerHTML = '';
    }
    if (field === "bday")
    {
        var name = document.getElementById(field).value;
        var today = new Date();
        var birthDate = new Date(name);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        //alert(birthDate);
        if(age < 15 || !name.match(/[0-1][0-9]\/[0-3][0-9]\/[0-9]{4}/) || isNaN(birthDate.getMonth()))
                document.getElementById(query).innerHTML = "Invalid Date";
        else
            document.getElementById(query).innerHTML = "";
    }
}