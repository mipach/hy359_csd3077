"use strict";

var TIV3077  =function() {
	var files;
	var latitude;
	var longitude;
        
        function getImages(x){
            document.getElementById('exifinfo').style.visibility = "hidden";
            document.getElementById('exifinfo').innerHTML = "";
            var xhr = new XMLHttpRequest();
            xhr.open('POST','GetImage');
            xhr.responseType = "blob";
            xhr.onload = function(){
            var blob = xhr.response;
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onload = function(){
            var base64Data = reader.result;
            
            document.getElementById('container').innerHTML += "<img class = 'smallimage' src=\""+base64Data+"\" style=\"width:140px;height:140px;\" onmouseout=\"TIV3077.clearMetaData();\" onmouseover=\"TIV3077.getMetaData("+x+",\'onhover\');\" onclick=\"TIV3077.showImage("+x+",\'container\');\">";
            };
        };
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send("img_id="+x+"&metadata=false");
    }
    function getImage(x,elem){
            var xhr = new XMLHttpRequest();
            xhr.open('POST','GetImage');
            xhr.onload = function(){
                var text = xhr.responseText;
                document.getElementById(elem).innerHTML = text;
            };
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send("img_id="+x+"&metadata=true");
    }
        
	return{
		loadImages: function(){
			    var xhr = new XMLHttpRequest();
                            var num = document.getElementById("number_of_images").value;
                            num = parseInt(num);
                            xhr.open('POST','GetImageCollection');
                            xhr.onload = function(){
                            if(xhr.readyState === 4 && xhr.status === 200)
                            {
                                files = JSON.parse(xhr.responseText);
                                //alert(files);
                            }
                            else if(xhr.status !== 200)
                            {
                                alert('Request failed. Returned status of'+ xhr.status);
                            }
                        };
                        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
                        xhr.send("number="+num);
		},
		getLoadedImages: function(){
			return files;
		},
                getMetaData:function(id,elem){
                  getImage(id,elem);  
                },
                clearMetaData:function()
                {
                  document.getElementById('onhover').innerHTML="";  
                },
		showLoadedImages: function(elem){
			//if there is an error message remove it
                        document.getElementById('ajaxContent').style.display = 'block';
       			document.getElementById("container").innerHTML = "";
			document.getElementById('exifinfo').setAttribute("style","hidden");
			document.getElementById("map").innerHTML = "";
			//document.getElementById("map").style = "width:400px;height:200px;";
			var ele = document.getElementById('no_images');
			if (ele) {
				ele.remove();
			}
			//if no images print the error message
			if (files === undefined) {
				var divv = document.createElement('div');
				divv.id = "no_images";
				divv.innerHTML = ["<h1>NO IMAGES LOADED</h1>"];
				document.getElementById(elem).insertBefore(divv,null);
			}
			else{
				//print the images
                                var i;
                                for(i = 0;i<files.length;i++)
                                {
                                    getImages(files[i]);
                                }
			}
		},
                unregisterUser: function()
                {
                  TIV3077.loadImages();
                  setTimeout(function(){TIV3077.showLoadedImages('intropics');},1000);
                
                },
		showImage: function(index,elem){
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST','GetImage');
                        xhr.responseType = "blob";
                        xhr.onload = function(){
                            if(xhr.readyState ===4 && xhr.status === 200){
                            //alert("onload xhr");
                            document.getElementById('onhover').innerHTML='';
                            var blob = xhr.response;
                            var reader = new FileReader();
                            reader.readAsDataURL(blob);
                            //alert(blob);
                            reader.onload = function () {
                               
                                var base64Data = reader.result;
                                document.getElementById('container').innerHTML = "<img src=\""+base64Data+"\" id=\"largeImage\" onclick=\"TIV3077.showLoadedImages('container');\">";
                                document.getElementById('ajaxContent').style.display = 'none';
                                document.getElementById("map").style = "width:400px;height:200px;";
                                TIV3077.showImageDetailedExifInfo(base64Data);
                                
                                //document.getElementById('map').innerHTML = TIV3077.showImageDetailedExifInfo(index);

                            };
                        }
                        else
                        {
                            alert("Request failed"+xhr.status);
                        }

                    };
                    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
                    xhr.send("img_id="+index+"&metadata=false");
		},
		showImageDetailedExifInfo: function(base64Data){
                    var img = document.createElement("IMG");
                    img.setAttribute("src",base64Data);
                    
                    EXIF.getData(img, function() {
                       var allMetaData = EXIF.getAllTags(this);
                       document.getElementById('exifinfo').innerHTML = JSON.stringify(allMetaData, null, "\t");
                       document.getElementById('exifinfo').style.visibility = "visible";
                    });
                    TIV3077.showImageDetailedExifWithMap(img);
		},
		initMap: function() {
			  	var uluru = {lat:Number(latitude[2]),lng:Number(longitude[2])}; 
			  	 var map = new google.maps.Map(document.getElementById("map"),{zoom:4,center:uluru});
			  	 var marker = new google.maps.Marker({position: uluru,map:map});
		},
		showImageDetailedExifWithMap: function (img) {
                    EXIF.getData(img,function () {
			
                        //var latRef = EXIF.getTag(img,"GPSLatitudeRef");
                        //var longRef = EXIF.getTag(img,"GPSLongitudeRef");
                        
                        longitude = EXIF.getTag(img,"GPSLongitude");
                        latitude = EXIF.getTag(img,"GPSLatitude");
                        
		  	var script = document.createElement('script');
		  	script.src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuBaiB4jOBIYzMXeKl12ibCN6fAsvRK0I&callback=TIV3077.initMap";
		  	document.getElementById('map').appendChild(script);	
                    });
		}

	};
}();