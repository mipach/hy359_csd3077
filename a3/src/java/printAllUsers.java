/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ask3.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Michalis
 */
@WebServlet(name = "printAllUsers", urlPatterns = {"/printAllUsers"})
public class printAllUsers extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
                out.println("<h1>Change your information</h1><br>");
                out.println("<label for='passwd'>Password:</label> <input id='passwd_l' type='password' name='Passwd' pattern='^(?=.*\\d)(?=.*[!@#$%^&*()_])(?=.*[a-zA-Z]).{5,10}$' required><br>"+
                               "<label for='vpasswd'>Verify Password:</label> <input id='vpasswd_l' type='password' name='VPasswd' ><br>"+
                               "<label for='mail'>E-mail:</label>   <input id='mail_l' type='email' name='Mail' ><br>"+
                               "<label for='name'>Name:</label> <input id='name_l' type='text' name=\"Name\"  pattern=\"[a-zA-zΑ-Ωα-ω]{2,20}\"><br>"+
                               "<label for='lname'>Last Name:</label> <input id='lname_l' type='text' name=\"LName\"  pattern=\"[a-zA-zΑ-Ωα-ω]{3,20}\"><br>"+
                               "<label for='bday'>Birth date:</label> <input type='text' id=\"bday_l\" name='bday' pattern=\"[0-9]{2}/[0-9]{2}/[0-9]{4}\" ><br>"+
                               "<label for='HomeTown'>HomeTown:</label>  <input id='HomeTown_l' type='text' name=\"hometown\" pattern=\".{1,50}\"><br>"+
                               "<label for='information'>More information:</label> <input id='information_l' type='text' name=\"Information\"  maxlength='500' ><br>"+
                            "<input type=\"button\" value=\"Update\" onclick=\"sendUpdate();\">"+
                            "<input type='button' value='print Details' onclick='printDetails();'>"+
                            "<input type='button' value='print Users' onclick='printAllUsers();'>");
            if(request.getParameter("PrintAllUsers").equals("true"))
            {
                out.println("<br>");
                out.println( Register.db.printAllUsers());
            }
           // out.println("<input type=\"text\" value=\""+request.getParameter("username_logged")+"\" name=\"username_logged\" id='username_l' hidden>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
