/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ask3;

import java.util.HashMap;
/**
 *
 * @author Michalis
 */
public class UserDB {
    private HashMap<String,User> database;
    
    public UserDB(){
        database = new HashMap<>();
    }
    
    public boolean containsUser(String name)
    {
        return database.containsKey(name);
    }
    
    public void add_logged(User user)
    {
        database.put(user.getUsername(),user);
    }
    
    public boolean add(User user){
        if(database.containsKey(user.getUsername())){
            return false;
        }
        else{
            database.put(user.getUsername(),user);
        }
        return true;
    }
    
    public boolean checkUserExist(User user)
    {
        if(database.containsKey(user.getUsername()))
        {
            User tmp = (User)database.get(user.getUsername());
            if(tmp.getPassword().equals(user.getPassword()))
                return true;
        }
        return false;
    }
    
    public User getUser(String name)
    {
        return database.get(name);
    }
    
    public String printAllUsers()
    {
        StringBuilder all = new StringBuilder();
        for (User us: database.values())
        {
            all.append(us.getUsername());
            all.append("<br>");
        }
        return all.toString();
    }
}
