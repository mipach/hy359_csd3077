/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ask3;

/**
 *
 * @author Michalis
 */
public class User {
    private String username;
    private String password;
    private String email;
    private String FirstName;
    private String LastName;
    private String birthday;
    private String gender;
    private String country;
    private String hometown;
    private String information;
    
    public User(String uname,String passwd,String mail,String fname,String lname,String bday,String gend,String cntr,String town,String inf)
    {
        username = uname;
        password = passwd;
        email    = mail;
        FirstName= fname;
        LastName = lname;
        birthday = bday;
        gender   = gend;
        country  = cntr;
        hometown = town;
        information = inf;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getCountry() {
        return country;
    }

    public String getHometown() {
        return hometown;
    }

    public String getInformation() {
        return information;
    }
    
}
