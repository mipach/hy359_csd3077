/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function deleteUser()
{
    var xhr = new XMLHttpRequest();
    xhr.open('POST','DeleteUser');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('onhover').innerHTML='';
            document.getElementById('exifinfo').innerHTML='';
            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            document.getElementById('exifinfo').style.display = "hidden";
            document.getElementById('register_form').style.display = 'block';
            document.getElementById('container').innerHTML = "";
            document.getElementById('keys').innerHTML = "";
            document.getElementById('comment').innerHTML='';
            document.getElementById('map').innerHTML = '';
            document.getElementById('map').style.display = 'none';
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send();
}