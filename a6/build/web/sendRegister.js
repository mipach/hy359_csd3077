/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';

function sendRegister(){
    var uname = document.getElementById('uname').value;
    var mail = document.getElementById('mail').value;
    var passwd = document.getElementById('passwd').value;
    var vpasswd = document.getElementById('vpasswd').value;
    var name = document.getElementById('name').value;
    var lname = document.getElementById('lname').value;
    var bday = document.getElementById('bday').value;
    var gender = document.getElementById('gender').value;
    var country = document.getElementById('country').value;
    var hometown = document.getElementById('HomeTown').value;
    var information = document.getElementById('information').value;
    
    var enpasswd = passwd+"ssalt";
    var envpasswd = vpasswd+"ssalt";
    
    var today = new Date();
    var birthDate = new Date(bday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
         age--;
    }
    if(age > 15 && bday.match(/[0-1][0-9]\/[0-3][0-9]\/[0-9]{4}/))
    {
    var xhr = new XMLHttpRequest();
    xhr.open('POST','Register');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send("UName="+uname+"&Mail="+mail+"&Passwd="+enpasswd+"&VPasswd="+envpasswd+"&Name="+name+"&LName="+lname+"&bday="+bday+"&gender="+gender+"&country="+country+"&hometown="+hometown+"&Information="+information);
    }
    else
    {
        document.getElementById('ajaxContent').innerHTML = "please compete correct the register form";
    }
}

function sendLogin()
{
    var name = document.getElementById('username').value;
    var pass = document.getElementById('password').value;
    var encryptedpass = pass + "ssalt";
    //alert(encryptedpass);
     var xhr = new XMLHttpRequest();
    xhr.open('POST','Login');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('exifinfo').style.display = "hidden";
            document.getElementById('container').innerHTML = '';
            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            document.getElementById('onhover').innerHTML = '';
            //alert(xhr.responseText);
            if(xhr.responseText !== "There is no such users.Please register or try to login again"){
            document.getElementById('register_form').style.display = 'none';
           // document.getElementById('keys').innerHTML = "<input id=\"images\" type=\"file\" webkitdirectory mozdirectory directory name=\"myFiles\" onchange=\"TIV3077.loadImages();\" on multiple/><button id=\"disp\" type=\"button\" onclick=\"TIV3077.showLoadedImages('container');\">Display</button></form>";
            document.getElementById('keys').innerHTML = "Image title:<input type='text' value='title' id='imagetitle'/><input type='file' name='photo' id='photo'><input type='button' value='Submit Image' onClick='sendImage();'>";
            //document.getElementById('keys2').innerHTML = "<input type='button' value='Load my Gallery' onclick='TIV3077.loadImages();'>";
            //document.getElementById('keys2').innerHTML +="   ";
            //document.getElementById('keys2').innerHTML += "<input type='button' value='Display Gallery' onclick='TIV3077.showLoadedImages(\"container\");'>"
            }
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send("username="+name+"&password="+encryptedpass);
}

function sendUpdate()
{
    var passwd = document.getElementById('passwd_l').value;
    var vpasswd = document.getElementById('vpasswd_l').value;
    var mail = document.getElementById('mail_l').value;
    var name = document.getElementById('name_l').value;
    var lname = document.getElementById('lname_l').value;
    var bday = document.getElementById('bday_l').value;
    var inf = document.getElementById('information_l').value;
    var ht = document.getElementById('HomeTown_l').value;
    
    
    var xhr = new XMLHttpRequest();
    xhr.open('POST','Update');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('exifinfo').style.display = "hidden";
            document.getElementById('onhover').innerHTML = '';
            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send("Mail="+mail+"&Passwd="+passwd+"&VPasswd="+vpasswd+"&Name="+name+"&LName="+lname+"&bday="+bday+"&hometown="+ht+"&Information="+inf);
}

function printDetails()
{
    var xhr = new XMLHttpRequest();
    xhr.open('POST','PrintAll');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('exifinfo').style.display = "hidden";
            document.getElementById('onhover').innerHTML = '';
            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send("PrintUser=true");
}

function printAllUsers()
{
    var xhr = new XMLHttpRequest();
    xhr.open('POST','printAllUsers');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('exifinfo').style.display = "hidden";
            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send("PrintAllUsers=true"); 
}


function sendLogout(){
    var xhr = new XMLHttpRequest();
    xhr.open('POST','Logout');
    xhr.onload = function(){
        if (xhr.readyState ===4 && xhr.status === 200)
        {
            document.getElementById('onhover').innerHTML='';
            document.getElementById('exifinfo').innerHTML='';
            document.getElementById('ajaxContent').innerHTML = '';
            document.getElementById('exifinfo').style.display = "hidden";
            document.getElementById('register_form').style.display = 'block';
            document.getElementById('container').innerHTML = "";
            document.getElementById('comment').innerHTML='';
            document.getElementById('keys').innerHTML = "";
            document.getElementById('map').innerHTML = '';
            document.getElementById('map').style.display = 'none';
            TIV3077.unregisterUser();
        }
        else if (xhr.status !== 200)
        {
            alert('Request failed. Returned status of '+ xhr.status);
        }
    };
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.send();
}
