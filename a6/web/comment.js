/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function postComment(index)
{
    //document.getElementById('exifinfo').style.visibility = "hidden";
      var com = document.getElementById('comment_box').value;
      var xhr = new XMLHttpRequest();
      xhr.open('POST','SetComment');
      xhr.onload = function(){
        if(xhr.readyState === 4 && xhr.status === 200)
        {
           // files = JSON.parse(xhr.responseText);
            //alert(files);
            //  alert(xhr.responseText);
        }
        else if(xhr.status !== 200)
        {
            alert('Request failed. Returned status of'+ xhr.status);
        }
     };
      xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
      xhr.send("comment="+com+"&index="+index);
}

function getComments(index){
         //var com = document.getElementById('comment_box').value;
      var xhr = new XMLHttpRequest();
      xhr.open('POST','GetComments');
      xhr.onload = function(){
        if(xhr.readyState === 4 && xhr.status === 200)
        {
           // files = JSON.parse(xhr.responseText);
            document.getElementById('comment').innerHTML = xhr.responseText;
            //  alert(xhr.responseText);
        }
        else if(xhr.status !== 200)
        {
            alert('Request failed. Returned status of'+ xhr.status);
        }
     };
      xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
      xhr.send("index="+index);
    
}
function deleteComment(id,index){
    var xhr = new XMLHttpRequest();
      xhr.open('POST','DeleteComment');
      xhr.onload = function(){
        if(xhr.readyState === 4 && xhr.status === 200)
        {
           // files = JSON.parse(xhr.responseText);
            document.getElementById('comment').innerHTML = getComments(index);
            //  alert(xhr.responseText);
        }
        else if(xhr.status !== 200)
        {
            alert('Request failed. Returned status of'+ xhr.status);
        }
     };
      xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
      xhr.send("id="+id+"&index="+index);
}

function updateComment(id,index){
        var xhr = new XMLHttpRequest();
        var com = document.getElementById('new_comment').value;
      xhr.open('POST','UpdateComment');
      xhr.onload = function(){
        if(xhr.readyState === 4 && xhr.status === 200)
        {
           // files = JSON.parse(xhr.responseText);
            document.getElementById('comment').innerHTML = getComments(index);
            //  alert(xhr.responseText);
        }
        else if(xhr.status !== 200)
        {
            alert('Request failed. Returned status of'+ xhr.status);
        }
     };
      xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
      xhr.send("id="+id+"&index="+index+"&comment="+com);
}