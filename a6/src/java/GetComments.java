/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cs359db.db.CommentDB;
import cs359db.db.PhotosDB;
import cs359db.db.RatingDB;
import cs359db.model.Comment;
import cs359db.model.Photo;
import cs359db.model.Rating;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Michalis
 */
@WebServlet(urlPatterns = {"/GetComments"})
public class GetComments extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
             HttpSession session = request.getSession(false);
            if(session != null){
                      
            if((String)session.getAttribute("username_logged") != null)
            {
                String usr = (String)session.getAttribute("username_logged");
                    int image_id = Integer.parseInt(request.getParameter("index"));
                    List<Comment> comments = CommentDB.getComments(image_id);
                    int num = 0;
                    int k =0;
                    Photo photo = PhotosDB.getPhotoMetadataWithID(image_id);
                    String photo_user = photo.getUserName();
                    for(Comment c: comments)
                    {
                        //if(r.getUserName().equals((String)session.getAttribute("username_logged"))){
                        // num = r.getRate();
                        //}
                        
                        if(c.getUserName().equals(usr)){
                         out.print(c.getUserName()+"("+c.getTimestamp()+"): <br>"+c.getComment()+"<input type='button' value='Delete' onclick='deleteComment("+c.getID()+","+image_id+");'"+
                                "<br>"+ "<input id=\"new_comment\" type=\"text\">"+
                        "<input type='button' value='Edit' onclick='updateComment("+c.getID()+","+image_id+");')><br>"+ "<br><br>");  
                        }
                        else if((usr.equals(photo_user))){
                        out.print(c.getUserName()+"("+c.getTimestamp()+"): <br>"+c.getComment()+"<input type='button' value='Delete' onclick='deleteComment("+c.getID()+","+image_id+");'"+
                                "<br><br>");}
                        else{
                            out.print(c.getUserName()+"("+c.getTimestamp()+"): <br>"+c.getComment()+
                                "<br><br>");}
                    }
//                    out.print("Your rating is: "+num+"<br>Total ratings: "+k);
            }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetComments.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetComments.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
