/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import cs359db.db.PhotosDB;
import cs359db.db.RatingDB;
import cs359db.model.Photo;
import cs359db.model.Rating;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Michalis
 */
@WebServlet(urlPatterns = {"/SetRating"})
public class SetRating extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            HttpSession session = request.getSession(false);
            if(session != null){  
                int image_id = Integer.parseInt(request.getParameter("index"));
                int rat = Integer.parseInt(request.getParameter("rating"));
                if((String)session.getAttribute("username_logged") != null)
                {
                    Rating rating = new Rating();
                    rating.setPhotoID(image_id);
                    rating.setRate(rat);
                    rating.setUserName((String)session.getAttribute("username_logged"));
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    //System.out.println(dateFormat.format(date));
                    rating.setTimestamp(dateFormat.format(date));
                    rating.checkFields();
                    //RatingDB.addRating(rating);
                    List<Rating> ratings = RatingDB.getRatings(image_id);
                    int sum =0;
                    int num = 0;
                    int flag = 0;
                    for(Rating r: ratings)
                    {
                        if(r.getUserName().equals((String)session.getAttribute("username_logged"))){
                            flag = 1;
                        }
                         sum += r.getRate();
                         num++;
                    }
                    if(flag == 0)
                    {
                        RatingDB.addRating(rating);
                    }
                    else
                    {
                        RatingDB.deleteRating(rating);
                        RatingDB.addRating(rating);
                        //RatingDB.updateRating(rating);
                    }
                    int overall = sum / num;
                    PhotosDB.getPhotoMetadataWithID(image_id).setNumberOfRatings(num);
                    out.print("bhka"+sum+" "+overall+" "+num);
                }
                else
                {
                    out.print("den bhka");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SetRating.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SetRating.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SetRating.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SetRating.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
